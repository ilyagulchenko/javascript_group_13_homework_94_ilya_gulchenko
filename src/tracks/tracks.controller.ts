import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
  constructor(
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
  ) {}

  @Get()
  fetchTracks(@Query('album') album: string) {
    if (!album) {
      return this.trackModel.find();
    }
    return this.trackModel.find({ album: album }).populate('album', 'title');
  }

  @Post()
  addNewTrack(@Body() trackDto: CreateTrackDto) {
    const track = new this.trackModel({
      title: trackDto.title,
      album: trackDto.album,
      is_published: trackDto.is_published,
      length: trackDto.length,
    });

    return track.save();
  }

  @Delete(':id')
  async removeTrack(@Param('id') id: string) {
    await this.trackModel.deleteOne({ _id: id });

    return { message: `Track deleted: ${id}` };
  }
}
