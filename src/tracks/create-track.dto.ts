export class CreateTrackDto {
  title: string;
  album: string;
  is_published: boolean;
  length: string;
}
