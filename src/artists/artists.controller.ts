import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Artist, ArtistDocument } from '../schemas/artist.schema';
import { CreateArtistDto } from './create-artist.dto';

@Controller('artists')
export class ArtistsController {
  constructor(
    @InjectModel(Artist.name) private artistModel: Model<ArtistDocument>,
  ) {}

  @Get()
  fetchArtists() {
    return this.artistModel.find();
  }

  @Get(':id')
  fetchArtist(@Param('id') id: string) {
    return this.artistModel.findById(id);
  }

  @Post()
  addNewArtist(@Body() artistDto: CreateArtistDto) {
    const artist = new this.artistModel({
      name: artistDto.name,
      information: artistDto.information,
      is_published: artistDto.is_published,
      image: artistDto.image,
    });

    return artist.save();
  }

  @Delete(':id')
  async removeArtist(@Param('id') id: string) {
    await this.artistModel.deleteOne({ _id: id });

    return { message: `Artist deleted: ${id}` };
  }
}
