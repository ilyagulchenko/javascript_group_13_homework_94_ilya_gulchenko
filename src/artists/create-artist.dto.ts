export class CreateArtistDto {
  name: string;
  information: string;
  is_published: boolean;
  image: string;
}
