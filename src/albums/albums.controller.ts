import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { CreateAlbumDto } from './create-album.dto';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
  ) {}

  @Get()
  fetchAlbums(@Query('artist') artist: string) {
    if (!artist) {
      return this.albumModel.find();
    }
    return this.albumModel
      .find({ artist: artist })
      .populate('artist', 'name information');
  }

  @Get(':id')
  fetchAlbum(@Param('id') id: string) {
    return this.albumModel.findById(id);
  }

  @Post()
  addNewAlbum(@Body() albumDto: CreateAlbumDto) {
    const album = new this.albumModel({
      artist: albumDto.artist,
      title: albumDto.title,
      date: albumDto.date,
      is_published: albumDto.is_published,
      image: albumDto.image,
    });

    return album.save();
  }

  @Delete(':id')
  async removeAlbum(@Param('id') id: string) {
    await this.albumModel.deleteOne({ _id: id });

    return { message: `Album deleted: ${id}` };
  }
}
