export class CreateAlbumDto {
  artist: string;
  title: string;
  date: string;
  is_published: string;
  image: string;
}
